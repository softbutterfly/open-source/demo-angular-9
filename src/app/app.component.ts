import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'my-app';
  pokemons: any[] = [];
  name: string = "";



  constructor(
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.getPokemons();
  }
  getPokemons() {
    this.http.get('http://127.0.0.1:3000').subscribe((values: any) => {

      if (!values.success) this.pokemons = []

      const { data } = values

      data.results.forEach((element: any) => {
        let pokemon_tmp = {
          name: String,
          img: String,
          hp: String,
          attack: String,
          defense: String,
          type: String
        };

        pokemon_tmp.name = element.name;
        this.getPokemon(element.name).subscribe((res: any) => {
          const { data: value } = res

          pokemon_tmp.img = value.sprites.other.dream_world.front_default;
          pokemon_tmp.hp = value.stats[0].base_stat;
          pokemon_tmp.attack = value.stats[1].base_stat;
          pokemon_tmp.defense = value.stats[2].base_stat;
          pokemon_tmp.type = value.types[0].type.name;
        });
        this.pokemons.push(pokemon_tmp);
      });
    });
  }

  getPokemon(name: string) {
    return this.http.get(`http://127.0.0.1:3000/${name}`)
  }

  getAllTypes() {
    this.http.get('https://pokeapi.co/api/v2/type/').subscribe((values => {
      console.log(values);
    }))
  }

  search() {
    if (this.name.toLowerCase() == "") {
      this.pokemons.length = 0;
      this.getPokemons();
      return;
    }
    this.getPokemon(this.name.toLowerCase()).subscribe((res: any) => {
      const { data: value } = res

      let pokemon_tmp = {
        name: String,
        img: String,
        hp: String,
        attack: String,
        defense: String,
        type: String
      };

      pokemon_tmp.name = value.forms[0].name;
      pokemon_tmp.img = value.sprites.other.dream_world.front_default;
      pokemon_tmp.hp = value.stats[0].base_stat;
      pokemon_tmp.attack = value.stats[1].base_stat;
      pokemon_tmp.defense = value.stats[2].base_stat;
      pokemon_tmp.type = value.types[0].type.name;

      this.pokemons.length = 0;
      this.pokemons.push(pokemon_tmp);

    })
  }
}
